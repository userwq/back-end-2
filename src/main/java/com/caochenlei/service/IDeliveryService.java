package com.caochenlei.service;

import com.caochenlei.VO.RecruitmentVo;
import com.caochenlei.entity.delivery;

import java.util.List;

/**
 * 投递表service层
 * 9-9日21点
 * 添加者：张锋
 * 1.添加了增加一条投递记录
 */
public interface IDeliveryService {
    default public int addOneDelivery(Integer stuId,Integer recuiId, delivery delivery){
        return 0;
    }

    /**
     * （黄勇+）
     * 查询某条招聘信息的投递简历，携带其简历的学生信息和学校信息
     * @param id
     * @return
     */
    List<delivery> queryRecruitmentResumeStudens(Integer id);
    List<RecruitmentVo> queryRecruitmentResumeStudens2(Integer id);
}

