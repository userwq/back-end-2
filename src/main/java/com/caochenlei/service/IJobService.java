package com.caochenlei.service;

import com.caochenlei.entity.Jobs;

import java.util.List;

public interface IJobService {
    //查找所有招聘岗位
    public List<Jobs> findAll();
}
