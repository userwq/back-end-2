package com.caochenlei.service;


import com.caochenlei.entity.Recruitment;

import java.util.List;

/**
 * 9-8 0点
 * 修改者：张锋
 * 补充了分页查询招聘信息方法（且根据工种）
 */
public interface IRecruitmentService {
    //查找一个招聘信息
    public Recruitment findOne(Integer id);

    //查找所有招聘信息
    public List<Recruitment> findAll(Integer tid);

    //添加一个招聘信息
    public Integer add(Recruitment recruitment);

    //删除一个招聘信息
    public Integer delete(Integer id);

    //修改一个招聘信息
    public Integer update(Recruitment recruitment);

    /**
     * 分页查询招聘信息方法（且根据工种）
     * @param pageNum
     * @param pageSize
     * @param jobId
     * @return
     */
    default public List<Recruitment> findByPageWithJobType(Integer pageNum,Integer pageSize,Integer jobId){
        return  null;
    }

    /**
     * 查询数据总数的方法
     * @return
     */
    default public int findDataNum(Integer jobTypeId){
        return 0;
    }
}
