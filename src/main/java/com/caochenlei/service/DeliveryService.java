package com.caochenlei.service;

import com.caochenlei.VO.DeliveryVo1;

import java.util.List;

public interface DeliveryService {
    List<DeliveryVo1> query(Integer id);
    Integer del(Integer id);
    Integer total(Integer id);
}
