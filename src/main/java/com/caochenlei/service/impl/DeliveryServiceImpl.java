package com.caochenlei.service.impl;

import com.caochenlei.VO.RecruitmentVo;
import com.caochenlei.dao.IDeliveryDao;
import com.caochenlei.entity.delivery;
import com.caochenlei.mapper.DeliveryMapper;
import com.caochenlei.service.IDeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 投递表service层
 * 9-9日21点
 * 添加者：张锋
 * 1.添加了增加一条投递记录
 */
@Service
public class DeliveryServiceImpl implements IDeliveryService {
    @Autowired
    IDeliveryDao deliveryDao;

    @Autowired
    DeliveryMapper deliveryMapper;

    @Override
    public int addOneDelivery(Integer stuId,Integer recuiId, delivery delivery) {
        if(deliveryDao.queryByStuId(stuId,recuiId)){
            return 0;
        }else{
            return deliveryDao.insertOne(delivery);
        }
    }
    /**
     * （黄勇+）
     * 查询某条招聘信息的投递简历，携带其简历的学生信息和学校信息
     * @param id
     * @return
     */
    @Override
    public List<delivery> queryRecruitmentResumeStudens(Integer id) {
        return deliveryMapper.queryRecruitmentResumeStudens(id);
    }

    @Override
    public List<RecruitmentVo> queryRecruitmentResumeStudens2(Integer id) {
        return deliveryMapper.queryRecruitmentResumeStudens2(id);
    }
}
