package com.caochenlei.service.impl;

import com.caochenlei.dao.IResumeDao;
import com.caochenlei.entity.Resume2;
import com.caochenlei.entity.resume;
import com.caochenlei.service.IResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 *  * 9-8 0点
 *  * 添加者：张锋
 *  * 添加了根据学生id查询对应简历
 */

@Service
public class ResumeServiceImpl implements IResumeService {
    @Autowired
    private IResumeDao iResumeDao;
    /**
     * 根据学生id查询对应简历
     * @param studentId
     * @return
     */
    @Override
    public List<resume> getStudentResumes(Integer studentId) {
       return iResumeDao.queryResumeByStuId(studentId);
    }
    /**
     * 根据简历id删除简历
     * 添加者 唐元元 9-10
     * @param id
     * @return
     */
    @Override
    public int deleteId(Integer id) {
        return iResumeDao.deleteId(id);
    }

    @Override
    public int addOneResume(Resume2 resume) {
        return iResumeDao.addOneResume(resume);
    }
}
