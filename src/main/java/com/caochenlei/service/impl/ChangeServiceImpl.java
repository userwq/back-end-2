package com.caochenlei.service.impl;

import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;
import com.caochenlei.mapper.ChangeMapper;
import com.caochenlei.service.ChangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ChangeServiceImpl  implements ChangeService {
	@Autowired
	private ChangeMapper changeMapper;
	@Override
	public Integer changeS(Students students) {
		return changeMapper.changeS(students);
	}

	@Override
	public Integer changeT(Teacher teacher) {
		return changeMapper.changeT(teacher);
	}

	@Override
	public Students getStudents(Integer id) {
		return changeMapper.getStudents(id);
	}
	@Override
	public Students getStudents(String phone) {
		return changeMapper.getStudent(phone);
	}

	@Override
	public Teacher getTeacher(String phone) {
		return changeMapper.getTeacher(phone);
	}

}
