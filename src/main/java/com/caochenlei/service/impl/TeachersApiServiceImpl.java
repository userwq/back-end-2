package com.caochenlei.service.impl;

import com.caochenlei.dao.TeachersApiDao;
 import com.caochenlei.entity.Teacher;
import com.caochenlei.service.TeachersApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * 黄勇+
 */
@Service
public class TeachersApiServiceImpl implements TeachersApiService {
    @Autowired
    TeachersApiDao teachersApiDao;

    /**
     * 查询单个教师信息
     * @param id
     * @return
     */
    @Override
    public Teacher findById(Integer id) {
        return teachersApiDao.findById(id);
    }
     /**
     * 修改教师信息
     * @param teacher
     * @return
     */
    @Override
    public int update(Teacher teacher) {
        return teachersApiDao.update(teacher);
    }

    @Override
    public int delet(Integer id) {
        return teachersApiDao.delet(id);
    }

    @Override
    public List<Teacher> findAllTeacher() {
        return teachersApiDao.findAllTeacher();
    }
}
