package com.caochenlei.service.impl;

import com.caochenlei.dao.BaseDao;
import com.caochenlei.dao.IRecruitmentDao;

import com.caochenlei.dao.impl.RecruitmentImpl;
import com.caochenlei.entity.Recruitment;
import com.caochenlei.service.IRecruitmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 9-8 0点
 * 修改者：张锋
 * 补充了分页查询招聘信息方法（且根据工种）
 */
@Service
public class RecruitmentServiceImpl implements IRecruitmentService {
    @Autowired
    IRecruitmentDao recruitmentMapper;
    @Override
    public Recruitment findOne(Integer id) {
        return recruitmentMapper.findOne(id);
    }
   //显示所有招聘信息
    @Override
    public List<Recruitment> findAll(Integer tid) {
        return recruitmentMapper.findAll(tid);
    }

    @Override
    public Integer add(Recruitment recruitment) {
        return recruitmentMapper.add(recruitment);
    }

    @Override
    public Integer delete(Integer id) {
        return recruitmentMapper.delete(id);
    }

    @Override
    public Integer update(Recruitment recruitment) {
        return null;
    }


    /**
     * 分页查询招聘信息方法（且根据工种）
     * @param pageNum
     * @param pageSize
     * @param jobId
     * @return
     */
    @Override
    public List<Recruitment> findByPageWithJobType(Integer pageNum, Integer pageSize, Integer jobId) {
        return recruitmentMapper.findByPageAndJobType(pageNum,pageSize,jobId);
    }

    /**
     * 查询数据总数的方法
     * @return
     */
    @Override
    public int findDataNum(Integer jobTypeId) {
        return recruitmentMapper.queryDataSum(jobTypeId);
    }
}
