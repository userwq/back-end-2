package com.caochenlei.service.impl;

import com.caochenlei.dao.JodDao;
import com.caochenlei.entity.Jobs;
import com.caochenlei.service.IJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class JobServiceImpl implements IJobService {
    @Autowired
    JodDao jodDao;
    @Override
    public List<Jobs> findAll() {
        return jodDao.findAll();
    }
}
