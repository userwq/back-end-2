package com.caochenlei.service.impl;

import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;


import com.caochenlei.mapper.RegisteredMapper;
import com.caochenlei.service.RegisteredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class RegisteredServiceImpl  implements RegisteredService {
	@Autowired
	private RegisteredMapper registeredMapper;

	@Override
	public Integer addStudent(Students students) {
		return registeredMapper.addStudent(students);
	}

	@Override
	public Integer addTeacher(Teacher teacher) {
		return registeredMapper.addTeacher(teacher);
	}

	@Override
	public Integer updateStudent(Students students) {
		return registeredMapper.updateStudent(students);
	}

	@Override
	public List<Students> checkPhone(Students students) {
		return registeredMapper.checkPhone(students);
	}

	@Override
	public List<Teacher> check(Teacher teacher) {
		return registeredMapper.check(teacher);
	}

	@Override
	public Students getPhone(String phone) {
		return registeredMapper.getPhone(phone);
	}


}
