package com.caochenlei.service.impl;

import com.caochenlei.dao.BaseDao;
import com.caochenlei.entity.Administrator;
import com.caochenlei.entity.Students;
import com.caochenlei.mapper.UserMapper;
import com.caochenlei.entity.User;
import com.caochenlei.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserMapper userMapper;
    @Autowired
    private BaseDao baseDao;


    @Override
    public User findOne(Integer id) {
        return userMapper.findOne(id);
    }

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public Integer save(User user) {
        return userMapper.save(user);
    }

    @Override
    public Integer delete(Integer id) {
        return userMapper.delete(id);
    }

    @Override
    public Integer update(User user) {
        return userMapper.update(user);
    }

    @Override
    public Administrator byId(Integer id) {
        return baseDao.byId(id);
    }


}
