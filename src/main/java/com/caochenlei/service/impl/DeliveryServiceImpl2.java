package com.caochenlei.service.impl;

import com.caochenlei.VO.DeliveryVo1;
import com.caochenlei.mapper.DetailsMapper;
import com.caochenlei.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DeliveryServiceImpl2  implements DeliveryService {
    @Autowired
    private DetailsMapper detailsMapper;
    @Override
    public List<DeliveryVo1> query(Integer id) {
        return detailsMapper.query(id);
    }

    @Override
    public Integer del(Integer id) {
        return detailsMapper.del(id);
    }

    @Override
    public Integer total(Integer id) {
        return detailsMapper.total(id);
    }
}
