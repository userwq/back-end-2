package com.caochenlei.service.impl;

import com.caochenlei.entity.Students;
import com.caochenlei.mapper.StudentManagement;
import com.caochenlei.service.StudentManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * 黄勇（后台学生管理）
 */
@Service
@Transactional
public class StudentManagementImpl implements StudentManagementService {
//    自动注入mybatis的类似dao成功
    @Autowired
    StudentManagement studentManagement;

    @Override
    public List<Students> findAllStudents() {
        return studentManagement.findAllStudents();
    }

    @Override
    public int update(Students students) {
        return studentManagement.update(students);
    }

    @Override
    public int delete(Integer id) {
        return studentManagement.delete(id);
    }
}
