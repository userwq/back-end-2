package com.caochenlei.service.impl;

import com.caochenlei.VO.SearchV0;
import com.caochenlei.entity.Recruitment;
import com.caochenlei.mapper.RecruitmentMapper;
import com.caochenlei.service.IRecruitmentService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class IRecruitmentService2Impl implements IRecruitmentService2 {
    @Autowired
    RecruitmentMapper recruitmentMapper;
    @Override
    public List<Recruitment> queryresuerment(Integer id, Integer pageNum, Integer pagesize) {
        int r=(pageNum-1)*pagesize;
        return recruitmentMapper.queryresuerment(id,r,pagesize);
    }

    @Override
    public int number(Integer id) {
        return recruitmentMapper.number(id);
    }

    @Override
    public int insertRecruitment(Recruitment recruitment) {
        return recruitmentMapper.insertRecruitment(recruitment);
    }
    //教师端模糊查询搜索 武文旭
    @Override
    public List<Recruitment> findByAll(SearchV0 searchV0) {
        return recruitmentMapper.findByAll(searchV0);
    }
}
