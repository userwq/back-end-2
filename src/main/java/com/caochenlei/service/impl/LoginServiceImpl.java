package com.caochenlei.service.impl;

import com.caochenlei.entity.Administrator;
import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;
import com.caochenlei.mapper.LoginMapper;
import com.caochenlei.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {
	@Autowired
	private LoginMapper loginMapper;
	@Override
	public Students loginStudent(Students students) {
		return loginMapper.loginStudent(students);
	}

	@Override
	public Teacher loginTeacher(Teacher teacher) {
		return loginMapper.loginTeacher(teacher);
	}

	@Override
	public Administrator loginAdministrator(Administrator administrator) {
		return loginMapper.loginAdministrator(administrator);
	}
}
