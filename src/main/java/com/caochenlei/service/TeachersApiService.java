package com.caochenlei.service;

import com.caochenlei.entity.Teacher;

import java.util.List;
/**
 * 黄勇+
 */
public interface TeachersApiService {
    /**
     * 查询
     */
    public Teacher findById(Integer id);
    /**
     * 修改教师信息
     */
    public int update(Teacher teacher);
    /**
     * 删除单个教师
     * @param id
     * @return
     */
    public int delet(Integer id);
    /**
     * 查询所有教师信息
     * @return
     */
    public List<Teacher> findAllTeacher();
}
