package com.caochenlei.service;

import com.caochenlei.entity.Resume2;
import com.caochenlei.entity.resume;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  * 9-8 0点
 *  * 添加者：张锋
 *  * 添加了根据学生id查询对应简历
 */

public interface IResumeService {
    /**
     * 根据学生id查询对应简历
     * @param studentId
     * @return
     */
    default public List<resume>  getStudentResumes(Integer studentId){
        return null;
    }
    /**
     * 根据简历id删除简历
     * 添加者 唐元元 9-10
     * @param id
     * @return
     */
    public int deleteId(Integer id);
    /**
     * 增加一条简历
     */
    default public int addOneResume(Resume2 resume){
        return 0;
    }

}
