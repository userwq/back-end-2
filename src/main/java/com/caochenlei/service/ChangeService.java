package com.caochenlei.service;

import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;

public interface ChangeService {
	public Integer changeS(Students students);
	public Integer changeT(Teacher teacher);
	public Students getStudents(Integer id);
	public Students getStudents(String phone);
	Teacher getTeacher(String phone);
}
