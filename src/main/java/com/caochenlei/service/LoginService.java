package com.caochenlei.service;

import com.caochenlei.entity.Administrator;
import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;

public interface LoginService {
	public Students loginStudent(Students students);
	public Teacher loginTeacher(Teacher teacher);
	public Administrator loginAdministrator(Administrator administrator);
}
