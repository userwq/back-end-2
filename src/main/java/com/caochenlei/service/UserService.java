package com.caochenlei.service;

import com.caochenlei.entity.Administrator;
import com.caochenlei.entity.Students;
import com.caochenlei.entity.User;

import java.util.List;

public interface UserService {
    //查找一个用户
    public User findOne(Integer id);

    //查找所有用户
    public List<User> findAll();

    //添加一个用户
    public Integer save(User user);

    //删除一个用户
    public Integer delete(Integer id);

    //修改一个用户
    public Integer update(User user);
    public Administrator byId(Integer id);

}
