package com.caochenlei.service;

import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;

import java.util.List;

public interface RegisteredService {
	public Integer addStudent(Students students);
	public Integer addTeacher(Teacher teacher);
	public Integer updateStudent(Students students);
	public List<Students> checkPhone(Students students );
	public  List<Teacher> check(Teacher teacher );
	Students getPhone(String phone);

}
