package com.caochenlei.service;

import com.caochenlei.entity.Students;

import java.util.List;

/**
 * 黄勇（后台学生管理）
 */
public interface StudentManagementService {
    /**
     * 查询所有状态为2的教师
     * @return
     */
    List<Students> findAllStudents();
    /**
     * 修改学生状态
     */
    int update(Students students);
    /**
     * 删除学生
     * @param id
     * @return
     */
    int delete(Integer id);
}
