package com.caochenlei.entity;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = -445352327778902151L;

    private Integer uid;
    private String uname;
    private String ugender;
    private Integer uage;

    public User() {
    }

    public User(Integer uid, String uname, String ugender, Integer uage) {
        this.uid = uid;
        this.uname = uname;
        this.ugender = ugender;
        this.uage = uage;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUgender() {
        return ugender;
    }

    public void setUgender(String ugender) {
        this.ugender = ugender;
    }

    public Integer getUage() {
        return uage;
    }

    public void setUage(Integer uage) {
        this.uage = uage;
    }
}