package com.caochenlei.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;


public class Resume2 {
    private Integer id;
    private String name;
    private  String type;
    private String content;
    @JsonProperty("local_studet")
    private Integer localStudet;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("upload_time")
    private Date uploadTime;
    private List<Delivery2> delivery2;
    public Resume2(){}
    public Resume2(Integer id, String name, String type, String content, Integer localStudet, Date uploadTime, List<Delivery2> delivery2) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.content = content;
        this.localStudet = localStudet;
        this.uploadTime = uploadTime;
        this.delivery2 = delivery2;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getLocalStudet() {
        return localStudet;
    }

    public void setLocalStudet(Integer localStudet) {
        this.localStudet = localStudet;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public List<Delivery2> getDelivery2() {
        return delivery2;
    }

    public void setDelivery2(List<Delivery2> delivery2) {
        this.delivery2 = delivery2;
    }
}
