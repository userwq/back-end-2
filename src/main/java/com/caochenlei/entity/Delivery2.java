package com.caochenlei.entity;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Delivery2 {
    @JsonProperty("id")
    private Integer d_id;
    @JsonProperty("recruitment_id")
    private Integer recruitmentId;
    @JsonProperty("resume_id")
    private Integer resumeId;
    private  Recruitment recruitment;

    public Integer getD_id() {
        return d_id;
    }

    public void setD_id(Integer d_id) {
        this.d_id = d_id;
    }

    public Integer getRecruitmentId() {
        return recruitmentId;
    }

    public void setRecruitmentId(Integer recruitmentId) {
        this.recruitmentId = recruitmentId;
    }

    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    public Recruitment getRecruitment() {
        return recruitment;
    }

    public void setRecruitment(Recruitment recruitment) {
        this.recruitment = recruitment;
    }
}
