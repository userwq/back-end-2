package com.caochenlei.entity;

import lombok.Data;

@Data
public class School {
	private Integer id;
	private String name;
}
