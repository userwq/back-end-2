package com.caochenlei.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 9-8 0点
 * 修改者：张锋
 * 补充了一个工种对象实体
 */
@Data
public class Recruitment {
	private Integer id;
	private String enterprise;
	private String content;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date releaseTime;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	@JsonProperty("latesttime")


	private Date latestTime;
	private Integer teacher;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer Number;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private  Integer jobs;
	@JsonProperty("job_object")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Jobs jobObject;
	//增加一个教师 一对一(黄勇)
	@JsonInclude(JsonInclude.Include.NON_NULL)
 	Teacher teachers;

	public Recruitment() {
	}




	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(String enterprise) {
		this.enterprise = enterprise;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(Date releaseTime) {
		this.releaseTime = releaseTime;
	}

	public Date getLatestTime() {
		return latestTime;
	}

	public void setLatestTime(Date latestTime) {
		this.latestTime = latestTime;
	}

	public Integer getTeacher() {
		return teacher;
	}

	public void setTeacher(Integer teacher) {
		this.teacher = teacher;
	}

	public Integer getNumber() {
		return Number;
	}

	public void setNumber(Integer number) {
		Number = number;
	}

	public Integer getJobs() {
		return jobs;
	}

	public void setJobs(Integer jobs) {
		this.jobs = jobs;
	}
}
