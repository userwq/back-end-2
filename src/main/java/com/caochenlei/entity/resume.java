package com.caochenlei.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *  * 9-8 0点
 *  * 修改者：张锋
 *  * 补充了属性:上传时间
 *    修改了属性：所属学生id：由String类型变更为Integer
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class resume {
    private Integer id;
    private String name;
    private  String type;
    private String content;
    private Integer localStudet;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("upload_time")
    private Date uploadTime;
    //一对一，简历所属学生（黄勇+）
    private Students students;


}
