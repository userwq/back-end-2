package com.caochenlei.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 投递实体
 */
@Data
public class delivery {
    private Integer id;
    @JsonProperty("recruitment_id")
    private Integer recruitmentId;
    @JsonProperty("resume_id")
    private Integer resumeId;
    //一对多，一个招聘信息对应多份简历(黄勇+)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<resume> resumes;

    public delivery() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRecruitmentId() {
        return recruitmentId;
    }

    public void setRecruitmentId(Integer recruitmentId) {
        this.recruitmentId = recruitmentId;
    }

    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    @Override
    public String toString() {
        return "delivery{" +
                "id=" + id +
                ", recruitmentId=" + recruitmentId +
                ", resumeId=" + resumeId +
                ", resumes=" + resumes +
                '}';
    }
}
