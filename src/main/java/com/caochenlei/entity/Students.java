package com.caochenlei.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Students {
	private Integer id;
	private String account;
	private String phone;
	private String email;
	private String name;
	private String gender;
	private String password;
	private String national;
	private String nativeplace;
	private String professional;
	private String education;
	@JsonProperty("local_school")
	private Integer localSchool;
	private Integer status;
	//	一对一学校
	@JsonInclude(JsonInclude.Include.NON_NULL)
	 School school;

	@Override
	public String toString() {
		return "Students{" +
				"id=" + id +
				", account='" + account + '\'' +
				", phone='" + phone + '\'' +
				", email='" + email + '\'' +
				", name='" + name + '\'' +
				", gender='" + gender + '\'' +
				", password='" + password + '\'' +
				", national='" + national + '\'' +
				", nativeplace='" + nativeplace + '\'' +
				", professional='" + professional + '\'' +
				", education='" + education + '\'' +
				", localSchool=" + localSchool +
				", status=" + status +
				", school=" + school +
				'}';
	}
}
