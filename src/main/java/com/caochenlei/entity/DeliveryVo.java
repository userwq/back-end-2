package com.caochenlei.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 9-11 11点
 * 添加者：张锋
 * 用于前端（学生端传回来的投递信息）接受
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryVo {
    @JsonProperty("stu_id")
    private Integer stuId;
    @JsonProperty("recu_id")
    private Integer recruitmentId;
    @JsonProperty("resume_id")
    private Integer resumeId;
}
