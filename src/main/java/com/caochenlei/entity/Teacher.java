package com.caochenlei.entity;

import lombok.Data;

@Data
public class Teacher {
	private Integer id;
	private String name;
	private String phone;
	private String password;
	private String email;
	private String account;
	private Integer status;
}
