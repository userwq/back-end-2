package com.caochenlei.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 跨域
 */
@WebFilter("/*")
public class CorsFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.addHeader("Access-Control-Allow-Origin", "*");//允许跨域的源
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");//跨域支持的方法
        response.addHeader("Access-Control-Allow-Headers", "Content-Type");//表示服务器允许请求携带的字段，可以写多个
        response.addHeader("Access-Control-Max-Age", "3600");//30 min//下次预检的缓存时间
        filterChain.doFilter(servletRequest, servletResponse);//是否允许携带凭证（tokei）  ture和*
    }

    @Override
    public void destroy() {

    }
}
