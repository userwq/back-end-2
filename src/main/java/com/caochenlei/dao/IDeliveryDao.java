package com.caochenlei.dao;

import com.caochenlei.entity.delivery;

/**
 * 投递表dao层
 * 9-8日15点
 * 添加者：张锋
 * 1.添加了根据学生id查询,招聘信息id是否已投递（防止重复投递）
 * 2.添加了增加一条投递记录
 */
public interface IDeliveryDao {
    /**
     * 根据学生id，招聘信息id查询是否已投递（防止重复投递）
     * 返回fals表示未投递
     * @return
     */
    default public boolean queryByStuId(Integer stuId,Integer recruiId){
        return false;
    }

    /**
     * 增加一条投递记录
     * @param delivery
     * @return
     */
    default public int insertOne(delivery delivery){
        return 0;
    }
}
