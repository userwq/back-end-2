package com.caochenlei.dao;

import com.caochenlei.entity.Administrator;

public interface BaseDao {
	public Administrator byId(Integer id);
}
