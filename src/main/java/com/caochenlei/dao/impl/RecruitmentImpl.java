package com.caochenlei.dao.impl;

import com.caochenlei.VO.SearchV0;
import com.caochenlei.dao.BaseDao;
import com.caochenlei.dao.IRecruitmentDao;
import com.caochenlei.entity.Jobs;
import com.caochenlei.entity.Recruitment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

//教师端招聘信息 武文旭
@Repository
public class RecruitmentImpl implements IRecruitmentDao {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 根据id查询招聘信息
     *
     * @param id
     * @return
     */
    @Override
    public Recruitment findOne(Integer id) {
        String spl = "select * from recruitment  where  id=? ";
        Object[] params = new Object[]{id};
        Recruitment rec = jdbcTemplate.queryForObject(spl, params, new RowMapper<Recruitment>() {
            @Override
            public Recruitment mapRow(ResultSet rs, int i) throws SQLException {
                Recruitment rec = new Recruitment();
                rec.setId(rs.getInt("id"));
                rec.setEnterprise(rs.getString("enterprise"));
                rec.setContent(rs.getString("content"));
                rec.setReleaseTime(new java.util.Date(rs.getTimestamp("releaseTime").getTime()));
                rec.setLatestTime(new java.util.Date(rs.getTimestamp("latestTime").getTime()));
                rec.setTeacher(rs.getInt("teacher"));
                rec.setNumber(rs.getInt("Number"));
                return rec;
            }
        });
        return rec;
    }

    //根据教师id查询所有招聘信息且携带岗位信息 武文旭
    @Override
    public List<Recruitment> findAll(Integer tid) {
        String spl = "SELECT  r.* ,j.name  FROM recruitment r LEFT JOIN jobs j ON r.jobs=j.id LEFT JOIN " +
                "teacher t ON  r.teacher=t.id WHERE t.id=?   ";
        List<Recruitment> recruitments = jdbcTemplate.query(spl, new RowMapper<Recruitment>() {
            @Override
            public Recruitment mapRow(ResultSet rs, int i) throws SQLException {
                Recruitment recruitments = new Recruitment();
                recruitments.setId(rs.getInt("id"));
                recruitments.setContent(rs.getString("content"));
                recruitments.setReleaseTime((Date) rs.getTimestamp("releasetime"));
                recruitments.setLatestTime((Date) rs.getTimestamp("latesttime"));
                recruitments.setEnterprise(rs.getString("enterprise"));
                Jobs jobs = new Jobs();
                jobs.setId(rs.getInt("jobs"));
                jobs.setName(rs.getString("name"));
                recruitments.setJobObject(jobs);
                return recruitments;
            }
        }, tid);
        return recruitments;
    }

    //添加招聘信息一条
    @Override
    public Integer add(Recruitment recruitment) {
        String sql = "INSERT INTO recruitment (enterprise,content,latesttime,teacher,jobs)  VALUES (?,?,?,?,?) ";
        Object[] params = new Object[]{recruitment.getEnterprise(),recruitment.getContent(),recruitment.getLatestTime(),recruitment.getTeacher(),recruitment.getJobs()};
        int i = jdbcTemplate.update(sql, params);
        return i;
    }

    //删除一条
    @Override
    public Integer delete(Integer id) {
        String sql = "delete from recruitment  where id=?";
        Object[] params = new Object[]{id};
        int i = jdbcTemplate.update(sql, params);
        return i;
    }

    @Override
    public Integer update(Recruitment recruitment) {
        return null;
    }

    //教师端模糊查询招聘信息
    @Override
    public List<Recruitment> findByAll(SearchV0 searchV0) {

        return null;
    }
    /**
     * 分页查询招聘信息方法（且根据工种）
     * @param pageNum
     * @param pageSize
     * @param jobTypeId
     * @return
     */
    @Override
    public List<Recruitment> findByPageAndJobType(Integer pageNum, Integer pageSize, Integer jobTypeId) {
        String  sql =null;
        Object[] params = null;
        if(jobTypeId==1){
            sql="select r.*,j.name from recruitment  r LEFT JOIN jobs j ON r.jobs=j.id  ORDER BY releasetime desc limit ?,? ";
            params = new Object[]{(pageNum-1)*pageSize,pageSize};
        }else {
            sql="select r.*,j.name from recruitment r LEFT JOIN jobs j ON r.jobs=j.id  where  jobs = ?   ORDER BY releasetime desc limit ?,? ";
            params = new Object[]{jobTypeId,(pageNum-1)*pageSize,pageSize};
        }

        List<Recruitment> recruitments = jdbcTemplate.query(sql, new RowMapper<Recruitment>() {
            @Override
            public Recruitment mapRow(ResultSet rs, int i) throws SQLException {
                Recruitment recruitments =new Recruitment();
                recruitments.setId(rs.getInt("id"));
                recruitments.setContent(rs.getString("content"));
                recruitments.setReleaseTime( rs.getTimestamp("releasetime"));
                recruitments.setLatestTime(rs.getTimestamp("latesttime"));
                recruitments.setEnterprise(rs.getString("enterprise"));
                Jobs job=new Jobs();
                job.setId(rs.getInt("jobs"));
                job.setName(rs.getString("name"));
                recruitments.setJobObject(job);
                return recruitments;
            }
        },params);
        return recruitments;
    }

    /**
     * 查询数据总数，便于分页
     * @return
     */
    @Override
    public int queryDataSum(Integer recuiTypeId) {
        //这种效率高 暂时不用
        // String sql ="select table_name,table_rows from information_schema.tables  where TABLE_SCHEMA = 'resume_collecting' and table_name='recruitment'";
        String sql = null;
        if(recuiTypeId==1){
            sql="select count(jobs) num from recruitment where 1=?";
        }else {
            sql="select count(jobs) num from recruitment where jobs = ?";
        }
        List<Integer> nums = jdbcTemplate.query(sql, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                int num = resultSet.getInt("num");
                return num;
            }
        },recuiTypeId);
        return nums.get(0);
    }
}