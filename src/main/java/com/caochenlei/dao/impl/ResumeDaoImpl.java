package com.caochenlei.dao.impl;

import com.caochenlei.dao.IResumeDao;
import com.caochenlei.entity.Resume2;
import com.caochenlei.entity.resume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
/**
 *  * 9-8 0点
 *  * 添加者：张锋
 *  * 添加了根据学生id查询对应简历
 */

@Repository
public class ResumeDaoImpl implements IResumeDao {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 根据学生id查询对应简历
     * @param studentId
     * @return
     */
    @Override
    public List<resume> queryResumeByStuId(Integer studentId) {
        String sql = "select * from resume where local_studet= ?";
        List<resume> resumes = jdbcTemplate.query(sql, new RowMapper<resume>() {
            @Override
            public resume mapRow(ResultSet resultSet, int i) throws SQLException {
                resume resume = new resume();
                resume.setId(resultSet.getInt("id"));
                resume.setName(resultSet.getString("name"));
                resume.setType(resultSet.getString("type"));
                resume.setContent(resultSet.getString("content"));
                resume.setUploadTime(resultSet.getTimestamp("upload_time"));
                System.out.println((Date) resume.getUploadTime());
                resume.setLocalStudet(resultSet.getInt("local_studet"));
                return resume;
            }
        },studentId);
        return resumes;
    }
    /**
     * 根据简历id删除简历
     * 添加者 唐元元 9-10
     * @param id
     * @return
     */
    @Override
    public int deleteId(Integer id) {
        String sql="DELETE from resume WHERE id=?";
        Object[] params=new Object[]{id};
        int i=jdbcTemplate.update(sql,params);
        return i;
    }

    /**
     * 增加一条简历
     * @param resume
     * @return
     */
    @Override
    public int addOneResume(Resume2 resume) {
        String sql = "insert into resume(name,type,content,upload_time,local_studet) values   (?,?,?,?,?)";
        int i =  jdbcTemplate.update(sql,resume.getName(),resume.getType(),resume.getContent(),resume.getUploadTime(),resume.getLocalStudet());
        return i;
    }
}
