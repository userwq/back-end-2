package com.caochenlei.dao.impl;

import com.caochenlei.dao.JodDao;
import com.caochenlei.entity.Jobs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
@Repository
public class JodImpl implements JodDao {
    @Autowired
    JdbcTemplate jdbcTemplate;
    //教师端显示招聘信息岗位 武文旭
    @Override
    public List<Jobs> findAll() {
        String spl="select * from jobs  ";
       List<Jobs>  jobs = jdbcTemplate.query(spl, new RowMapper<Jobs>() {
           @Override
           public Jobs mapRow(ResultSet rs, int i) throws SQLException {
               Jobs jobs =new Jobs();
               jobs.setId(rs.getInt("id"));
               jobs.setName(rs.getString("name"));
               return jobs;
           }
       });
        return jobs;
    }
}
