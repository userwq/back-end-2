package com.caochenlei.dao.impl;

import com.caochenlei.dao.TeachersApiDao;
import com.caochenlei.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 * 黄勇+
 */
@Repository
public class TeachersApiDaoImpl implements TeachersApiDao {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 查询教师信息
     * @param id
     * @return
     */
    @Override
    public Teacher findById(Integer id) {
    String sql="select * from teacher where id=? ";
    Teacher teacher=jdbcTemplate.queryForObject(sql, new RowMapper<Teacher>() {
        @Override
        public Teacher mapRow(ResultSet rs, int i) throws SQLException {
            Teacher teacher1=new Teacher();
            teacher1.setId(rs.getInt("id"));
            teacher1.setName(rs.getString("name"));
            teacher1.setAccount(rs.getString("account"));
            teacher1.setPassword(rs.getString("password"));
            teacher1.setPhone(rs.getString("phone"));
            teacher1.setEmail(rs.getString("email"));
            teacher1.setStatus(rs.getInt("status"));
            return teacher1;
        }
    },id);
         return teacher;
    }

    /**
     * 修改教师信息
     * @param teacher
     * @return
     */
    @Override
    public int update(Teacher teacher) {
        String sql="UPDATE teacher SET name=?,phone=?,email=?,password=?,status=? where id=?";
        Object[] params=new Object[]{teacher.getName(),teacher.getPhone(),teacher.getEmail(),teacher.getPassword(),teacher.getStatus(),teacher.getId()};
        int i=jdbcTemplate.update(sql,params);
        return i;
    }
    /**
     * 删除单个教师
     * @param id
     * @return
     */

    @Override
    public int delet(Integer id) {
        String sql="delete from teacher where id=?";
        int i=jdbcTemplate.update(sql,id);
        return i;
    }

    /**
     * 查询所有状态为2的教师
     *
     * @return
     */
    @Override
    public List<Teacher> findAllTeacher() {
        String sql="select * from teacher WHERE status=2";
        List<Teacher> teachers=jdbcTemplate.query(sql, new RowMapper<Teacher>() {
            @Override
            public Teacher mapRow(ResultSet rs, int i) throws SQLException {
                Teacher teacher=new Teacher();
                teacher.setId(rs.getInt("id"));
                teacher.setName(rs.getString("name"));
                teacher.setAccount(rs.getString("account"));
                teacher.setPassword(rs.getString("password"));
                teacher.setPhone(rs.getString("phone"));
                teacher.setEmail(rs.getString("email"));
                teacher.setStatus(rs.getInt("status"));
                return teacher;
            }
        });
        return teachers;
    }

}
