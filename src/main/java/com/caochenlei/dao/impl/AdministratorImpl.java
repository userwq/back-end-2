package com.caochenlei.dao.impl;

import com.caochenlei.dao.BaseDao;
import com.caochenlei.entity.Administrator;
import com.caochenlei.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AdministratorImpl  implements BaseDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	@Override
	public Administrator byId(Integer id) {
		String sql = "select * from administrator where id=?";
		Administrator administrator = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Administrator>(Administrator.class), id);

		return administrator;
	}
}
