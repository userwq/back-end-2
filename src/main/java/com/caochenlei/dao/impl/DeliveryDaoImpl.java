package com.caochenlei.dao.impl;

import com.caochenlei.dao.IDeliveryDao;
import com.caochenlei.entity.delivery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
/**
 * 投递表dao层
 * 9-8日15点
 * 添加者：张锋
 * 1.添加了根据学生id查询,招聘信息id是否已投递（防止重复投递）
 * 2.添加了增加一条投递记录
 */
@Repository
public class DeliveryDaoImpl implements IDeliveryDao {
   @Autowired
    private  JdbcTemplate jdbcTemplate;

    @Override
    public boolean queryByStuId(Integer stuId, Integer recruiId) {
        String sql = " select  del.* from resume res  inner join delivery del on res.id =del.resume_id and del.recruitment_id=? where local_studet = ?";
        List<delivery> deliverys = jdbcTemplate.query(sql,new RowMapper<delivery>(){
            @Override
            public delivery mapRow(ResultSet resultSet, int i) throws SQLException {
                delivery delivery = new delivery();
                delivery.setId(resultSet.getInt("id"));
                return delivery;
            }
        },recruiId,stuId);
        if(deliverys.size()==0){
            return false;//未投递
        }
        return true;
    }

    @Override
    public int insertOne(delivery delivery) {
        String sql = "insert into delivery (id,recruitment_id,resume_id) values(?,?,?)";
        int i = jdbcTemplate.update(sql,delivery.getId(),delivery.getRecruitmentId(),delivery.getResumeId());
        return i;
    }
}
