package com.caochenlei.dao;

import com.caochenlei.VO.SearchV0;
import com.caochenlei.entity.Recruitment;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * 9-8 0点
 * 修改者：张锋
 * 补充了分页查询招聘信息方法（且根据工种）
 * 9-11 张锋修改
 * 增加了一个查询数据总数的方法
 */

public interface IRecruitmentDao {
    //查找一个招聘信息
    public Recruitment findOne(Integer id);

    //查找所有招聘信息
    public List<Recruitment> findAll(Integer tid);

    //添加一个招聘信息
    public Integer add(Recruitment recruitment);

    //删除一个招聘信息
    public Integer delete(Integer id);

    //修改一个招聘信息
    public Integer update(Recruitment recruitment);
   //后端模糊查询招聘信息
    List<Recruitment> findByAll( SearchV0 searchV0);
    /**
     * 分页查询，且根据工种
     * @return
     */
    default public List<Recruitment> findByPageAndJobType(Integer pageNum,Integer pageSize,Integer jobTypeId){
        return null;
    }
    /**
     * 查询数据总数的方法
     */
    default int  queryDataSum(Integer recuiTypeId){
        return 0;
    }
}
