package com.caochenlei.dao;

import com.caochenlei.entity.Jobs;

import java.util.List;

public interface JodDao {
    //查找所有招聘岗位
    public List<Jobs> findAll();
}
