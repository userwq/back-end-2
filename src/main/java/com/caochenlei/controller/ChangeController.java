package com.caochenlei.controller;

import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;
import com.caochenlei.handler.Result;
import com.caochenlei.service.ChangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/change")
public class ChangeController {
	@Autowired
	private ChangeService changeService;

	/**
	 * 更改学生的密码
	 * @param students
	 * @return
	 */
	@PutMapping("/students")
	public Result changes(@RequestBody Students students){
		Integer integer = changeService.changeS(students);
		return Result.success(200,"成功",integer);
	}

	/**
	 * 更改老师信息
	 * @param teacher
	 * @return
	 */
	@PutMapping("/teacher")
	public Result changeT(@RequestBody Teacher teacher){
		Integer integer = changeService.changeT(teacher);
		return Result.success(200,"成功",integer);
	}

	/**
	 * 返回当前学生的密码
	 * @param id
	 * @return
	 */
	@GetMapping("/students/{id}")
	public Result getPassword(@PathVariable("id") Integer id){
		Students students = changeService.getStudents(id);
		return Result.success(200,"成功",students);
	}

	/**
	 * 查询该学生id，以便忘记密码的修改
	 * @param phone
	 * @return
	 */
	@GetMapping("/getS")
	public Result getPhone(String phone){
		Students students = changeService.getStudents(phone);
		return new  Result(200,"成功",students);
	}
	/**
	 * 查询该老师id，以便忘记密码的修改
	 * @param phone
	 * @return
	 */
	@GetMapping("/getT")
	public Result get(String phone){
		Teacher teacher = changeService.getTeacher(phone);
		return new  Result(200,"成功",teacher);
	}

}
