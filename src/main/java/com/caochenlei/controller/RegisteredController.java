package com.caochenlei.controller;

import cn.hutool.core.util.RandomUtil;
import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;
import com.caochenlei.handler.Result;
import com.caochenlei.service.RegisteredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/Registered")
/**
 * 注册以及账号随机完成
 */
public class RegisteredController {
	@Autowired
	private RegisteredService registeredService;
	@PostMapping("/students")
	public Result addStudent(@RequestBody Students students){
		List<Students> students1 = registeredService.checkPhone(students);
		Result result=null;
		if (students1.size()>0){
			result=new Result(200,"手机号重复",students1);
		}
		else {
			//账号随机
			students.setAccount(RandomUtil.randomString("abcdefghijklmnopqlstuvwxyzABCDEFGHIJKLMNOPQLSTUVWXYZ123456",6));
			Integer integer = registeredService.addStudent(students);
			result=	new Result(200,"注册成功",integer);
		}
		return result;
	}
	@PostMapping("/teacher")
	public  Result addTeacher(@RequestBody Teacher teacher) {
		List<Teacher> check = registeredService.check(teacher);
		Result result=null;
		if (check.size()>0){
			result=new Result(200,"手机号重复",check);
		}
		else {
			teacher.setAccount(RandomUtil.randomString("abcdefghijklmnopqlstuvwxyzABCDEFGHIJKLMNOPQLSTUVWXYZ123456",6));
			Integer integer = registeredService.addTeacher(teacher);
			result=	new Result(200,"注册成功",integer);
		}
		return result;
	}
	@GetMapping("/phone")
	public  Result getId(  String phone){
		Students phone1 = registeredService.getPhone(phone);
		return new Result(200,"成功",phone1);


	}
	@PutMapping("/update")
	public  Result updateStudent(@RequestBody Students students){
		Integer integer = registeredService.updateStudent(students);
		return  new Result(200, "操作成功", integer);
	}


}
