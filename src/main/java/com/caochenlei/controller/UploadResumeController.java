package com.caochenlei.controller;

import com.caochenlei.handler.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * 上传接口的添加
 * 添加者:张锋
 * 9-11 17点
 */
@RestController
@RequestMapping("/api/v1")
public class UploadResumeController {
    @Value("${file.upload}")
    private String  serverFilePath;//目前这种方法读取不到，原因未排查出来
    @PostMapping("/student/upload")
    public Result uploadResume(
            @RequestParam("file") MultipartFile file
            ) throws IOException {
        System.out.println("正在上传");
        String newFileName = System.currentTimeMillis()+"-"+file.getOriginalFilename();
        File dir = new File(serverFilePath);
        if (!dir.exists()){
            dir.mkdir();
        }
        File newFile = new File(serverFilePath+File.separator+newFileName);
        file.transferTo(newFile);
        return Result.success("上传成功",newFileName);
    }

}
