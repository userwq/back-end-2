package com.caochenlei.controller;

import com.caochenlei.entity.Administrator;
import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;
import com.caochenlei.handler.Result;
import com.caochenlei.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/login")
public class LoginController {
	@Autowired
	LoginService loginService;
	@PostMapping("/students")
	public Result loginS(@RequestBody Students students){
		Students one = loginService.loginStudent(students);
		return new Result(200,"操作成功" , one);
	}
	//老师登录
	@PostMapping("/teacher")
	public Result findOne(@RequestBody Teacher teacher) {
		Teacher teacher1 = loginService.loginTeacher(teacher);
		return new Result(200, "操作成功", teacher1);
	}
	@PostMapping("/administrator")
	public Result findOne(@RequestBody Administrator administrator) {
		Administrator administrator1 = loginService.loginAdministrator(administrator);
		return new Result(200, "操作成功", administrator1);
	}
}
