package com.caochenlei.controller;

import com.caochenlei.entity.Students;
 import com.caochenlei.handler.Result;
import com.caochenlei.service.StudentManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用于后端管理员接口,学生审核（黄勇）
 */
@RestController
@RequestMapping("/api/v1")
public class StudentManagement {
    @Autowired
    StudentManagementService studentManagementService;

    /**
     * 查询所有状态为2的学生
     * @return
     */
    @GetMapping("/students")
    public Result queryAll(){
        List<Students> students=studentManagementService.findAllStudents();
        return Result.success("查询成功",students);
    }
    /**
     * 更改学生状态
     * @param students
     * @return
     */
    @PutMapping("/updates")
    public Result update(
            @RequestBody Students students
     ){
        int i=studentManagementService.update(students);
        System.out.println(students);
        return Result.success("更新成功",i);
    }
    @DeleteMapping("/deletes/{id}")
    public Result delete(
            @PathVariable("id") Integer id
    ){
        int i=studentManagementService.delete(id);
        return Result.success("删除成功",i);
    }
}
