package com.caochenlei.controller;

import com.caochenlei.VO.DeliveryVo1;
import com.caochenlei.handler.Result;
import com.caochenlei.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 完成学生投递信息的查询
 */
@RestController
@RequestMapping("/delivery")
public class DeliveryControllers {
    @Autowired
    private DeliveryService deliveryService;
        @GetMapping("/resume/{id}")
        public Result query(@PathVariable("id") Integer id){
            List<DeliveryVo1> query = deliveryService.query(id);
            return Result.success("成功",query);
        }
        @DeleteMapping("/del/{id}")
        public Result del(@PathVariable("id") Integer id){
            Integer del = deliveryService.del(id);
            return Result.success("成功",del);
        }
        @GetMapping("/sum/{id}")
        public Result get(@PathVariable("id") Integer id){
            Integer total = deliveryService.total(id);
//            System.out.println(total);
            return Result.success("成功",total);
        }

}
