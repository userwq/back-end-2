package com.caochenlei.controller;

import com.caochenlei.VO.RecruitmentVo;
import com.caochenlei.entity.DeliveryVo;
import com.caochenlei.entity.delivery;
import com.caochenlei.handler.Result;
import com.caochenlei.service.IDeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 投递表控制层
 * 9-9日21点
 * 添加者：张锋
 * 1.添加了增加一条投递记录
 */
@RestController
@RequestMapping("/api/v1")
public class DeliveryController {
    @Autowired
    IDeliveryService deliveryService;
    @PostMapping("/delivery")
    public Result addOneDelivery(
            @RequestBody DeliveryVo deliveryVo
    ){
//        {
//            "stu_id":7,
//                    "recu_id":1,
//                            "resume_id":7
//        }
        delivery delivery = new delivery();
        delivery.setRecruitmentId(deliveryVo.getRecruitmentId());
        delivery.setResumeId(deliveryVo.getResumeId());
        int i = deliveryService.addOneDelivery(deliveryVo.getStuId(),deliveryVo.getRecruitmentId(),delivery);
        return Result.success(200,"插入执行成功",i);
    }

    /**
     * 查询某条招聘信息的简历，和其简历的学生信息和学校信息
     * @param id
     * @return
     */
    @GetMapping("/deliveryshy/{id}")
    public Result queryRecruitmentResumeStudens(
            @PathVariable("id") Integer id
    ){
        System.out.println(id);
        List<delivery> deliveries=deliveryService.queryRecruitmentResumeStudens(id);
        System.out.println(deliveries);
        return Result.success("查询成功",deliveries);
    }
    @GetMapping("/deliveryshy2/{id}")
    public Result queryRecruitmentResumeStudens2(
            @PathVariable("id") Integer id
    ){
        System.out.println(id);
        List<RecruitmentVo> recruitmentVos=deliveryService.queryRecruitmentResumeStudens2(id);
        System.out.println(recruitmentVos);
        return Result.success("查询成功",recruitmentVos);
    }
}
