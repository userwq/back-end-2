package com.caochenlei.controller;

import com.caochenlei.entity.Jobs;
import com.caochenlei.entity.User;
import com.caochenlei.handler.Result;
import com.caochenlei.service.IJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class JobController {
    @Autowired
    IJobService iJobService;
    //显示招聘岗位
    @GetMapping("/jobs")
    public Result findAll() {
        List<Jobs> jobs = iJobService.findAll();
        return new Result(200, "操作成功", jobs);
    }

}
