package com.caochenlei.controller;

import com.caochenlei.entity.Recruitment;
import com.caochenlei.handler.Result;
import com.caochenlei.service.IRecruitmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/api/v1")
public class RecruitmentController {
    @Autowired
    IRecruitmentService recruitmentService;
    //  在当前教师下添加一条
    @PostMapping(value = "/recruitments" )
    public Result insert1(@RequestBody Recruitment recruitment){
        System.out.println(recruitment);
        int i= recruitmentService.add(recruitment);
        if(i==1){
        }
        return Result.success("插入成功",i);
    }
    //当前教师删除一条
    @DeleteMapping("/recruitments/{id}")
    public Result delete(@PathVariable("id")Integer id ){
        int i=  recruitmentService.delete(id);
        return Result.success("删除成功",null);
    }
    //显示当前教师下的所有招聘信息
    @GetMapping("/findAll/{tid}")
    public Result findAll( @PathVariable Integer tid) {
        List<Recruitment> recruitments = recruitmentService.findAll(tid);
        return new Result(200, "操作成功", recruitments);
    }

    /**
     * 添加者：张锋 分页查询
     * @param pageNum
     * @param pageSize
     * @param jobId
     * @return
     */
    @GetMapping("/recruitments/page")
    public  Result findbyPage(
            @RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
            @RequestParam(value ="pageSize",defaultValue = "4") Integer pageSize,
            @RequestParam(value = "jobId",defaultValue = "1") Integer jobId
    ){
        List<Recruitment> recruitments = recruitmentService.findByPageWithJobType(pageNum,pageSize,jobId);
        return Result.success("分页查询成功",recruitments);
    }

    /**
     * 添加者：张锋 查询数据总数
     * @param jobTypeId
     * @return
     */
    @GetMapping("/recruitments/num/{jobTypeId}")
    public Result getDataSum(
            @PathVariable("jobTypeId") Integer jobTypeId
    ){
        int num = recruitmentService.findDataNum(jobTypeId);
        return Result.success("数据总数查询成功",num);
    }
}
