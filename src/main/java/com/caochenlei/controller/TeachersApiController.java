package com.caochenlei.controller;


import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ZipUtil;
import com.caochenlei.VO.RecruitmentVo;
import com.caochenlei.entity.Teacher;
import com.caochenlei.handler.Result;
import com.caochenlei.service.TeachersApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.List;

@RestController
@RequestMapping("/api/teachersApi")
public class TeachersApiController {
    @Value("${file.upload}")
    private String  serverFilePath;

    @Autowired
    TeachersApiService teachersApiService;
    /**
     * 查询单个教师
     */
    @GetMapping("/teachers/{id}")
    public Result query(@PathVariable("id") Integer id){
        Teacher teacher=teachersApiService.findById(id);
         return Result.success("查询成功",teacher);
    }

    /**
     * 修改教师信息
     * @param id
     * @param teacher
     * @return
     */
    @PutMapping("/teachers2/{id}")
    public Result modif(
            @PathVariable("id") Integer id,
            @RequestBody Teacher teacher
    ){
        teacher.setId(id);
        int i=teachersApiService.update(teacher);
        return Result.success("修改成功",i);
    }
     /**
     * 删除单个教师
     * @param id
     * @return
     */
    @DeleteMapping("/teachers3/{id}")
    public Result delete(@PathVariable("id") Integer id){
        int i=teachersApiService.delet(id);
        return Result.success("删除成功",i);
    }

    /**
     * 查询所有状态为2的教师
     * @return
     */
    @GetMapping("/teachers4")
    public Result queryAll(){
        List<Teacher> teachers=teachersApiService.findAllTeacher();
        return Result.success("全部查询成功",teachers);
    }

    @PostMapping("/hh/{id}")
    public Result hh(
            @PathVariable("id") Integer id,
            @RequestBody List<RecruitmentVo> recruitmentVos
    ){
        System.out.println(id);
        System.out.println("***************************");
         System.out.println(recruitmentVos);

        //FileUtil.del("D:\\全部2");
        FileUtil.del(serverFilePath+"\\"+id);
        //java.io.File mkdir = FileUtil.mkdir("D:\\全部2");
        java.io.File mkdir = FileUtil.mkdir(serverFilePath+"\\"+id);
        for (RecruitmentVo recruitmentVo : recruitmentVos) {

            BufferedInputStream in = FileUtil.getInputStream(serverFilePath+"\\"+recruitmentVo.getContent());
            String substring = recruitmentVo.getContent().substring(recruitmentVo.getContent().indexOf("-") + 1);
            System.out.println(substring);
            BufferedOutputStream out = FileUtil.getOutputStream(serverFilePath+"\\"+id+"\\"+substring);
            IoUtil.copy(in, out, IoUtil.DEFAULT_BUFFER_SIZE);
          }
        java.io.File zip = ZipUtil.zip(serverFilePath+"\\"+id, serverFilePath+"\\"+id+".zip");
        String name = zip.getName();


        return new Result(200,"成功",name);

    }

}
