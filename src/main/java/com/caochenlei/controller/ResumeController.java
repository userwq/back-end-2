package com.caochenlei.controller;

import com.caochenlei.entity.Resume2;
import com.caochenlei.entity.resume;
import com.caochenlei.handler.Result;
import com.caochenlei.service.IResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 *  * 9-8 0点
 *  * 添加者：张锋
 *    简历接口
 *  * 添加了根据学生id查询对应简历
 */
@RestController
@RequestMapping("/api/v1")
public class ResumeController {
    @Autowired
    private IResumeService iResumeService;
    @GetMapping("/resumes/{student_id}")
    public Result getResumeByStuId(
            @PathVariable("student_id") Integer stuId
    ){
        List<resume> resumes = iResumeService.getStudentResumes(stuId);
        return Result.success("查询学生简历成功",resumes);
    }
    @DeleteMapping("/resume/{resume_id}")
    public Result deleteResumeById(
            @PathVariable("resume_id") Integer id
    ){
       int i=iResumeService.deleteId(id);
       return Result.success("删除简历成功",i);
    }
    @PostMapping("/resume")
    public Result addOneResume(
            @RequestBody Resume2 resume
    ){
        int i = iResumeService.addOneResume(resume);
        return Result.success("增加成功",i);
    }


}
