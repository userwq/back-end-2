package com.caochenlei.controller;

import com.caochenlei.VO.SearchV0;
import com.caochenlei.entity.Recruitment;
import com.caochenlei.handler.Result;
import com.caochenlei.service.IRecruitmentService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 测试查询某一个教师的发布招聘信息，并且按发布时间排序，和分页，并且携带投递人数
 */

@RestController
@RequestMapping("/api/v12")
public class RecruitmentController2 {
    @Autowired
    IRecruitmentService2 iRecruitmentService2;
    @GetMapping("/Recruitmen2/{id}")
    public Result queryresuerment(
            @PathVariable("id") Integer id,
            @RequestParam(value = "_page",defaultValue ="1",required = false) Integer _page,
            @RequestParam(value = "_limit",defaultValue ="2",required = false) Integer _limit
    ){
        System.out.println(id);
        List<Recruitment> recruitments=iRecruitmentService2.queryresuerment(id,_page,_limit);
//        循环得到每一个招聘信息的id,在根据简历id获取投递人数
         for (Recruitment r:recruitments){
             r.setNumber(iRecruitmentService2.number(r.getId()));
        }

         return Result.success("分页成功",recruitments);
    }

    /**
     * 插入一条招聘信息
     * @param recruitment
     * @return
     */
    @PostMapping("/insertss")
    public Result insertRecruitment(
            @RequestBody Recruitment recruitment
    ){
        System.out.println(recruitment);
       int i=iRecruitmentService2.insertRecruitment(recruitment);
         return Result.success("发布成功",i);
    }
    //模糊查询招聘信息 搜索功能
    @GetMapping("/recruitments/all")
    public Result queryall( @RequestParam(value ="teacherId" ,required = false )Integer teacherId,
                            @RequestParam(value ="enterprise" ,required = false )String enterprise,
                            @RequestParam(value ="jobName" ,required = false )String jobName
    ){
        SearchV0 searchV0=new SearchV0(teacherId,enterprise,jobName);
        List<Recruitment> recruitments  = iRecruitmentService2.findByAll(searchV0);
        for (Recruitment r:recruitments){
            r.setNumber(iRecruitmentService2.number(r.getId()));
        }
        return Result.success("查询成功",recruitments);
    }
    //模糊查询招聘信息 搜索功能
    @GetMapping("/recruitments/all1")
    public Result queryall1( SearchV0 searchV0
    ){
        System.out.println(searchV0);
        List<Recruitment> recruitments  = iRecruitmentService2.findByAll(searchV0);
        return Result.success("查询成功",recruitments);
    }
}
