package com.caochenlei.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
//搜索招聘信息VO  武文旭
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchV0 {
    private Integer teacherId;
    private String enterprise;
    private String jobName;

    public SearchV0(String enterprise, String jobName) {
   this.enterprise=enterprise;
   this.jobName=jobName;
    }
}
