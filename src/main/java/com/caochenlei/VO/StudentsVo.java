package com.caochenlei.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class StudentsVo {
    private Integer id;
    private String account;
    private String phone;
    private String email;
    private String name;
    private String gender;
    private String password;
    private String national;
    private String nativeplace;
    private String professional;
    private String education;
    @JsonProperty("local_school")
    private Integer localSchool;
    private Integer status;
    private String sName;
}
