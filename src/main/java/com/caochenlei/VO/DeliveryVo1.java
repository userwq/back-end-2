package com.caochenlei.VO;

import lombok.Data;

import java.util.Date;

@Data
public class DeliveryVo1 {
    private String name;
    private String type;
    private String content;
    private Integer deliveryId;
    private String enterprise;
    private Date releaseTime;
    private  Integer teacher;
    private String tName;
    private Integer jobs;
}
