package com.caochenlei.VO;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecruitmentVo {
    private Integer id;
    private String resumename;
    private  String type;
    private String content;
    private String phone;
    private String email;
    private  String jobname;
    private String studentsname;
    private String gender;
    private String national;
    private String nativeplace;
    private String professional;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String education;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String schoolname;
}
