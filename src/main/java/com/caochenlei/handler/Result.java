package com.caochenlei.handler;


public class Result<T> {
    Integer code;
    String msg;
    T date;
    public Result(Integer code, String msg, T date) {
        this.code = code;
        this.msg = msg;
        this.date = date;
    }

public Result(){};

    /**
     * 成功
     * @param message
     * @param data
     * @return
     */
    public static Result success(String message,Object data){
         return new Result(200,message,data);
    }

    /**
     * 重载成功
     * @param code
     * @param message
     * @param data
     * @return
     */
    public static Result success(Integer code,String message,Object data){
        return new Result(code,message,data);
    }

    /**
     * 失败
     * @param message
     * @param data
     * @return
     */
    public static Result fail(String message,Object data){
        return new Result(500,message,data);
    }

    /**
     * 重载失败
     * @param code
     * @param message
     * @param data
     * @return
     */
    public static Result fail(Integer code,String message,Object data){
        return new Result(code,message,data);
    }
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getDate() {
        return date;
    }

    public void setDate(T date) {
        this.date = date;
    }
}
