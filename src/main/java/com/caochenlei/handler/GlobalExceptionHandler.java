package com.caochenlei.handler;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 增强器，全局异常处理
 * @RestControllerAdvice
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler({Exception.class})
    @ResponseBody
    public Result myException(Exception ex){
         return Result.fail(500,"出现异常",ex.getMessage());
    }
}
