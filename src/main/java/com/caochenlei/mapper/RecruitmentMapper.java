package com.caochenlei.mapper;

import com.caochenlei.VO.SearchV0;
import com.caochenlei.entity.Recruitment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 查询某一个老师的发布招聘信息和分页
 */
public interface RecruitmentMapper {
//    查询招聘信息且分页
    List<Recruitment> queryresuerment(@Param("id") Integer id,@Param("row") Integer row, @Param("pagesize") Integer pagesize);
    //查询某一条招聘信息的投递简历人数
    int number(Integer id);

    //插入一条招聘信息
    int insertRecruitment(Recruitment recruitment);
    //后端模糊查询招聘信息 武文旭
    List<Recruitment> findByAll(SearchV0 searchV0);
}
