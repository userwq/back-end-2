package com.caochenlei.mapper;

import com.caochenlei.entity.Students;

import java.util.List;

/**
 * 用于后台管理界面学生管理（黄勇）
 */
public interface StudentManagement {
    /**
     * 查询所有状态为2的学生
     * @return
     */
    List<Students> findAllStudents();
    /**
     * 修改学生状态
     */
    int update(Students students);

    /**
     * 删除学生
     * @param id
     * @return
     */
    int delete(Integer id);
}
