package com.caochenlei.mapper;

import com.caochenlei.VO.StudentsVo;
import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;

import java.util.List;

public interface RegisteredMapper {
	public Integer addStudent(Students students);
	public Integer addTeacher(Teacher teacher);
	public Integer updateStudent(Students students);
	public List <Students> checkPhone(Students students );
	public  List<Teacher> check(Teacher teacher );
	Students getPhone(String phone);
	StudentsVo query(Integer id);

}
