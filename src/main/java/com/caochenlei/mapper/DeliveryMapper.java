package com.caochenlei.mapper;

import com.caochenlei.VO.RecruitmentVo;
import com.caochenlei.entity.delivery;

import java.util.List;

public interface DeliveryMapper {
    /**
     * 查询某条招聘信息的简历，和其简历的学生信息和学校信息
     * @param id
     * @return
     */
    List<delivery> queryRecruitmentResumeStudens(Integer id);
    List<RecruitmentVo> queryRecruitmentResumeStudens2(Integer id);



}
