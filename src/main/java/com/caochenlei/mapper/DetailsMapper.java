package com.caochenlei.mapper;


import com.caochenlei.VO.DeliveryVo1;

import java.util.List;

public interface DetailsMapper {
   List<DeliveryVo1>  query(Integer id);
   Integer del(Integer id);
   Integer total(Integer id);
}
