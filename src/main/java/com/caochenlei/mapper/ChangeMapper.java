package com.caochenlei.mapper;

import com.caochenlei.entity.Students;
import com.caochenlei.entity.Teacher;

public interface ChangeMapper {

	public Integer changeS(Students students);
	public Integer changeT(Teacher teacher);
	public Students getStudents(Integer id);
	public Students getStudent(String phone);
	Teacher getTeacher(String phone);
}
